#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  creaEstructura
#  
#  Copyright 2019 Leo TANA <ltanav@tutanota.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


import pathlib,os,sys
from pathlib import Path

# p = pathlib.Path('example_dir')

# p1 = pathlib.Path('example')

# print('Creating {}'.format(p))
# p.mkdir(exist_ok=True)
# p1.mkdir(exist_ok=True)
# Path('example').rename('ejemplo')
# nombreArch = Path('sal2.png')
# print('es   ',nombreArch.suffix)

#archivos = Path('.').glob('*.png')
RUTA = 'SHP/'
ANTERIOR = 'ingre'
DPA = '080450001006'
# Lista de nombres de capas
# ~ nuevo = 'CA04_'+ DPA + ''
nuevo = 'INGRESOS_'+ DPA + '_L'
# nuevo = 'MAN_'+ DPA + '_A'
# nuevo = 'SEC_'+ DPA + '_A'
# nuevo = 'SEC_'+ DPA + '_L'
# nuevo = 'ADICIONALES_'+ DPA + '_L'
# nuevo = 'RIO_'+ DPA + '_A'
# nuevo = 'RIO_'+ DPA + '_L'
# nuevo = 'VIV_'+ DPA + '_P'
# nuevo = 'VIA_RUTA_'+ DPA + '_A'
# nuevo = 'LOC_'+ DPA + '_P'
#
archivos = Path('.').glob(RUTA  + ANTERIOR + '.*')
i = 1
#
for archivo in archivos:
	Path(archivo).rename(RUTA  + '{}{}'.format(nuevo,archivo.suffix))
	print(archivo)
	i = i + 1

print('--> Renombrado exitoso')
print(pathlib.Path.cwd())
#
# print(os.path.abspath("mydir/myfile.txt"))
# print(os.path.splitext(os.path.basename('ruta1/salida.png'))[0])    
