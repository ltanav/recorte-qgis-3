# Recorte QGIS 3

"MAN"  =  attribute( @atlas_feature , 'MAN')  
"Layers_field" = attribute (@atlas_feature, 'coverage_layer_field_name')

Atributos de otras tablas:

    miatributo = iface.activeLayer().selectedFeatures()[0];miatributo["SEC18"]
    attribute(get_feature('LOC_170160', 'NOM_LOC', '@layer_name'), ''NOM_LOC')
    [% aggregate( 'communes','concatenate',"NAME",contains( $geometry, @map_extent_center),', ')%]

Para obtener el atributo asignado:

    attribute( $currentfeature, 'man' )

Para obtener la escala del mapa, poner Item ID: mapa1

    [% map_get(item_variables( 'map1' ), 'map_scale') %]
    QGIS [% concat( attribute(  @atlas_feature , 'OBJECTID'))% ]
    
En xmin:
    x_min(aggregate(layer:='layer', aggregate:= 'collect', expression:=bounds($geometry), filter:=intersects( @atlas_geometry , $geometry ))) 

Para composicion:

    projectInstance = QgsProject.instance()
    projectLayoutManager = projectInstance.layoutManager()
    projectLayoutManager.layouts()
    layout = projectLayoutManager.layouts()[0]   #chose layout
    referencemap = layout.referenceMap()
    referencemap.scale()


mapItem = layout.itemById('Map')

Para tipo:

left( aggregate('CA04_210652903001_A', 'concatenate',"man",',',intersects( $geometry, geometry(@parent) ) ),6)
left( aggregate(layer:='CA04_210652903001_A', aggregate:='concatenate', expression:="man", filter:=intersects( $geometry, geometry(@parent) ) ), 6 )


Para man:

left( aggregate('CA04_210652903001_A', 'concatenate',"man",',',intersects( $geometry, geometry(@parent) ) ),15)
left( aggregate(layer:='CA04_210652903001_A', aggregate:='concatenate', expression:="man", concatenator:=',', filter:=intersects( $geometry, geometry(@parent) ) ),15)

Para

aggregate(
 layer:= 'zoning',
 aggregate:='concatenate',
 expression:=zoning_sim,
 concatenator:=', ',
 filter:=contains($geometry, geometry(@parent))
 )
 
 aggregate(
 layer:= 'Zip_Codes',
 aggregate:='count',
 expression:=$id,
 filter:=touches($geometry, geometry(@parent))
 )
 
Un conjunto de recorte de QGIS 3

Un ejemplo en la consola de python QGIS 3.x:

    import processing
    
    params = {'INPUT':capaEntrada,
              'PREDICATE':0,
              'INTERSECT':QgsProcessingFeatureSourceDefinition(capaActiva.id(), True), # USE HERE THE SELECTED FEATURES
              'METHOD':0}
              
    processing.run("native:buffer", 
            {'INPUT': '/home/miusuario/SHP/MAN_080450001006_A.shp',
            'DISTANCE': -10.0, # Aqui cambiar para el desplazamiento CURVA
            'SEGMENTS': 10,
            'DISSOLVE': True,
            'END_CAP_STYLE': 0,
            'JOIN_STYLE': 1,
            'MITER_LIMIT': 10,
            'OUTPUT': '/home/miusuario/buffers5.shp'})
            
    params = {'INPUT': '/home/miusuario/SHP/MAN_080450001006_A.shp',
            'DISTANCE': -10.0, # Aqui cambiar para el desplazamiento CURVA
            'SEGMENTS': 10,
            'DISSOLVE': True,
            'END_CAP_STYLE': 0,
            'JOIN_STYLE': 1,
            'MITER_LIMIT': 10,
            'OUTPUT': '/home/miusuario/buffers5.
    processing.run("native:buffer", params)

    processing.run("native:buffer", 
            {'INPUT': '/home/miusuario/SHP/MAN_080450001006_A.shp',
            'DISTANCE': -10.0, # Aqui cambiar para el desplazamiento CURVA
            'SEGMENTS': 10,
            'DISSOLVE': True,
            'END_CAP_STYLE': 0,
            'JOIN_STYLE': 1,
            'MITER_LIMIT': 10,
            'OUTPUT': '/home/miusuario/buffers5.shp'})
                  
# Mapa de valores para TIPO

    <field name="tipo">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option name="DIVISION" value="DIVISION" type="QString"/>
              </Option>
              <Option type="Map">
                <Option name="ENTRADA" value="ENTRADA" type="QString"/>
              </Option>
              <Option type="Map">
                <Option name="INGRESO" value="INGRESO" type="QString"/>
              </Option>
              <Option type="Map">
                <Option name="ESTRUCTURA INTERNA" value="ESTRUCTURA" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>