*
 * recortesNoviembre.md
 * 
 * Copyright 2019  <Leo TANA>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 *
 *
 
2019-11-08    <Leo TANA>

 * Para man
 
aggregate(layer:='CA04'+substr( @layer_name ,9,14), aggregate:='concatenate', expression:="man", 
concatenator:=',', filter:=intersects( $geometry, geometry(@parent) ) )

 * Para tipo
 
aggregate(layer:='CA04'+substr( @layer_name ,9,14), aggregate:='concatenate', expression:="man", filter:=intersects( $geometry, geometry(@parent) ) ) 

 
 

